package com.main;

import java.awt.*;

public class Item extends GameObject{
    int ITEM_SIZE = 8;

    public Item(int x, int y, ID id) {
        super(x, y, id);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.white);
        g.fillOval(x-ITEM_SIZE/2, y-ITEM_SIZE/2, ITEM_SIZE, ITEM_SIZE);
    }
}
