package com.main;

import java.awt.*;
import java.io.*;
import java.util.LinkedList;

public class Mapa extends GameObject{

    LinkedList<GameObject> objects = new LinkedList<>();

    final int MAP_WIDTH  = (int) (Game.HEIGHT*0.8);
    final int MAP_HEIGHT = (int) (Game.HEIGHT*0.8);

    final int PAREDE_WIDTH = 2;

    int MAP_X_ZERO;
    int MAP_Y_ZERO;

    public Mapa(int x, int y, ID id) {
        super(x, y, id);
        MAP_X_ZERO = (Game.WIDTH - MAP_WIDTH)/2;
        MAP_Y_ZERO = (Game.HEIGHT - MAP_HEIGHT)/2;
    }

    public Mapa() {
        super(0,0, ID.Mapa);

        MAP_X_ZERO = (Game.WIDTH - MAP_WIDTH)/2;
        MAP_Y_ZERO = (Game.HEIGHT - MAP_HEIGHT)/2;
        addContorno(MAP_WIDTH, MAP_HEIGHT);
    }

    public Mapa(String arquivoMapa, String arquivoItens) {
        super(0,0, ID.Mapa);

        MAP_X_ZERO = (Game.WIDTH - MAP_WIDTH)/2;
        MAP_Y_ZERO = (Game.HEIGHT - MAP_HEIGHT)/4;

        addContorno(MAP_WIDTH, MAP_HEIGHT);
        addArquivo(arquivoMapa);
        addItens(arquivoItens);

    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tmpObj = objects.get(i);
            tmpObj.render(g);
        }
    }

    public void addObject(GameObject object) {
        objects.add(object);
    }

    public void removeParede(Parede parede) {
        objects.remove(parede);
    }

    public void clear() {
        for (int i = 0; i < objects.size(); i++) {
            objects.remove(i);
        }
    }

    // TODO nao fica centralizado
    private void addContorno(int map_width, int map_height) {

        addObject(new Parede(MAP_X_ZERO-2,MAP_Y_ZERO -2, ID.Parede, true,  2, MAP_HEIGHT +2));
        addObject(new Parede(MAP_X_ZERO,MAP_Y_ZERO-2, ID.Parede, false, 2, MAP_HEIGHT));
        addObject(new Parede(MAP_X_ZERO + MAP_WIDTH, MAP_Y_ZERO -2, ID.Parede, true,2, MAP_HEIGHT +2));
        addObject(new Parede(MAP_X_ZERO -2 ,MAP_Y_ZERO + MAP_HEIGHT, ID.Parede, false,2, MAP_HEIGHT + 4));
    }

    // Adiciona paredes lidas de um arquivo txt
    private void addArquivo(String arquivo) {
        try {
            File mapFile = new File(arquivo);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivo)));

            // ler 1.a linha para saber quantidade de divisoes
            String linha = reader.readLine();
            char [] chars;


            int divisoes = Integer.parseInt(linha);
            int tileSize = MAP_WIDTH/divisoes;

            Point tileCorner = new Point();
            Parede tmpParede;

            for (int i = 0; i < divisoes; i++) {
                // le proxima linha
                linha = reader.readLine();
                chars = linha.toCharArray();
                // itera pela linha adicionando paredes correspondentes
                for (int j = 0; j < chars.length; j++) {
                    tileCorner.x = MAP_X_ZERO + j * tileSize;
                    tileCorner.y = MAP_Y_ZERO + i * tileSize;


                    switch (chars[j]) {
                        case 'I' :
                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2, tileCorner.y,
                                                    ID.Parede, true,
                                                    PAREDE_WIDTH, tileSize);
                            addObject(tmpParede);
                            break;

                        case '_' :
                            tmpParede = new Parede(tileCorner.x, tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                                    ID.Parede, false,
                                                    PAREDE_WIDTH, tileSize);
                            addObject(tmpParede);
                            break;

                        case 'P' :
                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2, tileCorner.y,
                                                    ID.Parede, true,
                                                    PAREDE_WIDTH, tileSize/2);
                            addObject(tmpParede);

                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2,
                                                   tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                                    ID.Parede, false,
                                                    PAREDE_WIDTH, tileSize/2 + PAREDE_WIDTH);
                            addObject(tmpParede);
                            break;

                        case 'Q' :
                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2,
                                    tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                    ID.Parede, false,
                                    PAREDE_WIDTH, tileSize/2 + PAREDE_WIDTH);
                            addObject(tmpParede);

                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2,
                                                   tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                                    ID.Parede, true,
                                                    PAREDE_WIDTH, tileSize/2 + PAREDE_WIDTH);
                            addObject(tmpParede);
                            break;

                        case 'R' :
                            tmpParede = new Parede(tileCorner.x, tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                    ID.Parede, false,
                                    PAREDE_WIDTH, tileSize/2);
                            addObject(tmpParede);

                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2,
                                    tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                    ID.Parede, true,
                                    PAREDE_WIDTH, tileSize/2 + PAREDE_WIDTH);
                            addObject(tmpParede);

                            break;

                        case 'S' :
                            tmpParede = new Parede(tileCorner.x + tileSize/2 - PAREDE_WIDTH/2, tileCorner.y,
                                    ID.Parede, true,
                                    PAREDE_WIDTH, tileSize/2);
                            addObject(tmpParede);

                            tmpParede = new Parede(tileCorner.x, tileCorner.y + tileSize/2 - PAREDE_WIDTH/2,
                                    ID.Parede, false,
                                    PAREDE_WIDTH, tileSize/2);
                            addObject(tmpParede);
                            break;
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO implementar Metodo para adicionar comidinhas lendo um arquivo txt
    private void addItens(String arquivo) {
        try {
            File itemFile = new File(arquivo);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(arquivo)));

            // ler 1.a linha para saber quantidade de divisoes
            String linha = reader.readLine();
            int divisoes = Integer.parseInt(linha);
            int tileSize = MAP_HEIGHT/divisoes;

            char [] chars;
            Point tileCorner = new Point();
            Item tmpItem;

            for (int i = 0; i < divisoes; i++) {
                chars = reader.readLine().toCharArray();

                for (int j = 0; j < chars.length; j++) {
                    tileCorner.x = MAP_X_ZERO + j * tileSize;
                    tileCorner.y = MAP_Y_ZERO + i * tileSize;

                    switch (chars[j]){
                        case '*':
                            tmpItem = new Item(tileCorner.x, tileCorner.y, ID.Item);
                            addObject(tmpItem);
                    }
                }

            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
