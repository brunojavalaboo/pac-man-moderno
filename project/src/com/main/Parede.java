package com.main;

import java.awt.*;

public class Parede extends GameObject{
    private boolean vertical; // variavel que diz se a parede eh vertical (true) ou horizontal (false)
    private int width;
    private int height;

    public Parede(int x, int y, ID id, boolean vertical, int width, int height) {
        super(x, y, id);
        this.vertical = vertical;
        this.height = height;
        this.width = width;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.blue);

        if (vertical)
            g.fillRect(x,y,width,height);
        else
            g.fillRect(x,y,height,width);
    }
}
