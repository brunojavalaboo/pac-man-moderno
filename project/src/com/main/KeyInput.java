package com.main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

    private Handler handler;

    public KeyInput(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        for (int i = 0; i < handler.objects.size(); i++) {
            GameObject tmpObject = handler.objects.get(i);

            if (tmpObject.getId() == ID.Player) {
                // key events for player 1

                if ( (key == KeyEvent.VK_W) || (key == KeyEvent.VK_UP)) {tmpObject.setVelocity_y(-3); tmpObject.setVelocity_x(0);}
                if ( (key == KeyEvent.VK_S) || (key == KeyEvent.VK_DOWN)) {tmpObject.setVelocity_y(3); tmpObject.setVelocity_x(0);}
                if ( (key == KeyEvent.VK_D) || (key == KeyEvent.VK_RIGHT)) {tmpObject.setVelocity_x(3); tmpObject.setVelocity_y(0);}
                if ( (key == KeyEvent.VK_A) || (key == KeyEvent.VK_LEFT)) {tmpObject.setVelocity_x(-3); tmpObject.setVelocity_y(0);}

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        for (int i = 0; i < handler.objects.size(); i++) {
            GameObject tmpObject = handler.objects.get(i);

            if (tmpObject.getId() == ID.Player) {
                // key events for player 1

//                if ( (key == KeyEvent.VK_W) || (key == KeyEvent.VK_UP)) tmpObject.setVelocity_y(0);
//                if ( (key == KeyEvent.VK_S) || (key == KeyEvent.VK_DOWN)) tmpObject.setVelocity_y(0);
//                if ( (key == KeyEvent.VK_D) || (key == KeyEvent.VK_RIGHT)) tmpObject.setVelocity_x(0);
//                if ( (key == KeyEvent.VK_A) || (key == KeyEvent.VK_LEFT)) tmpObject.setVelocity_x(0);

            }
        }
    }
}
