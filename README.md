# Pac-Man-Moderno

Um jogo de Pac Man repensado para temas atuais.

MAC0321 - Laboratório de Programação Orientada a Objetos.

> https://youtube.com/playlist?list=PLTeQ2u81sjqfsFNWrUCIoqJZBSJrai8M7

> https://school.hbh7.com/pdfs/RPI/Erich%20Gamma%2C%20Richard%20Helm%2C%20Ralph%20Johnson%2C%20John%20M.%20Vlissides%20-%20Design%20Patterns_%20Elements%20of%20Reusable%20Object-Oriented%20Software%20%20-Addison-Wesley%20Professional%20%281994%29.pdf

## Fase 0
+ Criação do repositório

## Fase 1

### Requisitos

1. O mapa do jogo definindo o leiaute do tabuleiro deve ser definido em um arquivo .txt que seja facilmente editável manualmente por um ser humano. O mapa deverá conter as paredes e todos os elementos iniciais do jogo, i.e., onde ficam os adversários, o jogador e todos os outros elementos que dão pontos ao serem obtidos.

2. Utilizando o teclado, o jogador poderá se movimentar nas várias direções comendo pontinhos e pegando objetos no tabuleiro.

### Planejamento

Para gerar o mapa do jogo, será criada uma classe Parede que irá conter a informação de uma parede. Serão criadas diversas paredes em posições diferentes, especificadas por um arquivo de texto, formando um mapa. As listas de paredes, itens coletáveis e tudo relacionado ao mapa do jogo onde o jogador se move estarão contidas na classe Mapa.

### Relatório
#### Resultados

Foi criado o backend do jogo, com gameloop e a capacidade de renderizar objetos na tela. A partir daí, foi criado o código necessário para gerar um mapa a partir de um arquivo .txt, contendo em sua primeira linha o número de divisões para posicionar paredes no mapa, e em seguida caracteres indicando o tipo de parede, como especificado na Imagem 1. Também foi feito o código para adicionar itens ao jogo, também especificados por um arquivo .txt.

![Imagem 1](img/fase1/azulejos.png "Imagem 1")

*Imagem 1*

##### Exemplo

Exemplo de texto para geração de mapa (mapa/itens):

![Imagem 2](img/fase1/mapa_daora_txt.jpg "Imagem 2")

Mapa gerado a partir do exemplo:

![Imagem 3](img/fase1/mapa_daora2.jpg "Imagem 3")




#### Pontos positivos

#### Pontos negativos

#### Lições aprendidas

## Fase 2

### Requisitos

3. Utilize o padrão Strategy para definir diferentes algoritmos para o movimento dos adversários (fantasminhas).

4. Utilizando o padrão Fábrica Abstrata, implemente a possibilidade do jogo ter diferentes aparências (look-and-feel). Assim, facilmente, o jogador poderá escolher se o espaço do jogo terá uma aparência de supermercado em tempos de covid, velho oeste, espaço sideral ou os corredores do IME, por exemplo. Quando o usuário selecionar uma nova aparência, todos os elementos gráficos do jogo devem se redesenhar imediatamente.

### Resultados

## Fase 3

### Requisitos

5. Utilize o padrão State para representar as diferentes fases do jogo. Cada fase deve ser um pouquinho mais difícil que a anterior.

6. Utilize o padrão Decorator para acrescentar algum comportamento dinâmico em um jogador quando ele pega alguma coisa que lhe dá super-poderes durante alguns segundos (como o Mario do Super Mario Bros quando come o cogumelo e fica grande e ligadão).

### Resultados

## Fase 4

### Requisitos (bonus)

7. Implemente uma música de fundo diferente para cada uma das fases.

8. Eventos especiais devem ter efeitos sonoros interessantes e agradáveis (cuidado para não tornar o jogo insuportável).

9. Opcionalmente adicionar itens e melhorias extras interessantes.

### Resultados

